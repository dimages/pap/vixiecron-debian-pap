#!/bin/sh

exec >>/tmp/log.txt
exec 2>&1

echo "~~~~~~~~~ $(date) ~~~~~~~~~"
/usr/local/bin/php -q /var/www/html/scripts/jobs.php
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~"