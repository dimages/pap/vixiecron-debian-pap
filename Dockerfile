FROM php:7.0-fpm

ENV TZ="UTC"

# Platform php dependencies
RUN apt-get update && \
    apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libmcrypt-dev libpng-dev && \
    docker-php-ext-install -j$(nproc) mysqli pdo_mysql mbstring iconv mcrypt && \
    docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ && \
    docker-php-ext-install -j$(nproc) gd

# Install ioncube loader
RUN apt-get update && \
    apt-get install -y wget && \
    wget https://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz && \
    tar -zxvf ioncube_loaders_lin_x86-64.tar.gz && \
    mkdir /usr/local/ioncube && \
    cp ioncube/ioncube_loader_lin_7.0.so /usr/local/ioncube/ioncube_loader_lin_7.0.so && \
    echo "zend_extension = /usr/local/ioncube/ioncube_loader_lin_7.0.so" >> /usr/local/etc/php/php.ini && \
    rm ioncube_loaders_lin_x86-64.tar.gz && \
    rm -R ioncube

# https://github.com/redmatter/docker-vixie-cron
RUN cd / && \
    wget https://raw.githubusercontent.com/redmatter/docker-vixie-cron/master/cron-user.sh && \
    wget https://raw.githubusercontent.com/redmatter/docker-vixie-cron/master/start-cron.sh

# crontab.txt is consumed by the cron daemon's startup script (see start-cron.sh)
COPY crontab.txt /
COPY periodic-task.sh /

RUN ( \
        export DEBIAN_FRONTEND=noninteractive; \

        BUILD_DEPS=""; \
        APP_DEPS="bash cron sudo"; \

        # so that each command can be seen clearly in the build output
        set -e -x; \

        # update to pull package list from apt sources
        apt-get update; \
        apt-get install --no-install-recommends -y $BUILD_DEPS $APP_DEPS ; \

        # remove the ones that come with the package; no need for that
        rm -f /etc/cron.daily/* ; \

        mv /cron-user.sh /usr/bin/cron-user; \
        chmod ugo+x,go-w /start-cron.sh /usr/bin/cron-user ; \

        # remove packages that we don't need
        apt-get remove -y $BUILD_DEPS ; \
        apt-get autoremove -y ; \
        apt-get clean; \
        rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*; \
    )

ENTRYPOINT ["sudo", "-E", "/start-cron.sh"]